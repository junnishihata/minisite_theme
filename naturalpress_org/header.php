<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja" xml:lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=<?php bloginfo('charset'); ?>" />

<title><?php if(is_front_page()): ?><?php bloginfo('name'); ?> - <?php bloginfo('description'); ?><?php else: ?><?php the_title(); ?> ｜ <?php bloginfo('name'); ?><?php endif; ?></title>

<meta name="viewport" content="width=device-width,user-scalable=yes,initial-scale=1.0" />
<meta http-equiv="content-script-type" content="text/javascript" />
<meta http-equiv="content-style-type" content="text/css" />


<?php if(is_front_page()): ?>

	<?php if(get_option('meta-keyword-val')): ?>
	<meta lang="ja" name="keywords" content="<?= htmlspecialchars(get_option('meta-keyword-val')); ?>" />
	<?php endif; ?>

	<?php if(get_option('meta-desc-val')): ?>
	<meta lang="ja" name="description" content="<?= htmlspecialchars(get_option('meta-desc-val')); ?>" />
	<?php endif; ?>

<?php elseif(is_single()): ?>

	<?php if(get_post_meta($post->ID, 'metakey', true)): ?>
	<meta lang="ja" name="keywords" content="<?= htmlspecialchars(get_post_meta($post->ID, 'metakey', true)); ?>" />
	<?php endif; ?>

	<?php if(get_post_meta($post->ID, 'metadesc', true)): ?>
	<meta lang="ja" name="description" content="<?= htmlspecialchars(get_post_meta($post->ID, 'metadesc', true)); ?>" />
	<?php endif; ?>

<?php endif; ?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<?php if(is_front_page()): ?>
<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri(); ?>/lib/slick/slick.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri(); ?>/lib/slick/slick-theme.css" media="screen" />
<script type="text/javascript" src="<?= get_template_directory_uri(); ?>/lib/slick/slick.min.js"></script>
<script>
$(function(){
$('.recomArea').slick({
	infinite: true,
	dots: true,
	autoplay: true,
	autoplaySpeed: 2000,
	slidesToShow: 4,
	slidesToScroll: 1,
	pauseOnHover:true,
	responsive: [
    {
      breakpoint: 1030,
      settings: {
		slidesToScroll: 1,
		centerMode: true,
		variableWidth: true
      }
    }
  ]
});
}); 
</script>
<?php endif; ?>

<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri(); ?>/style.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri(); ?>/lib/css/margin.css" media="all" />


<?php wp_head(); ?>

</head>

<body<?php if(is_single() || is_page()): ?> id="detailpage"<?php else: ?> id="listPage"<?php endif; ?>>

<?php if(get_option('google-analytics-val')): ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', '<?= htmlspecialchars(get_option('google-analytics-val')); ?>', 'auto');
  ga('send', 'pageview');

</script>
<?php endif; ?>

<a name="pagetop" id="pagetop"></a>


<!-- ===== head start ===== -->
<div id="headArea" class="clearfix">
<div id="catchArea"><p class="wrap"><?= htmlspecialchars(get_option('catch-copy-val')); ?></p></div>

<div class="logoArea wrap clearfix">
<<?php if(is_front_page()): ?>h1<?php else: ?>p<?php endif; ?> id="logo"><a href="<?= home_url(); ?>/"><?php if(get_option('theme-logo-val')): ?><img src="<?= htmlspecialchars(get_option('theme-logo-val')) ?>" alt="<?php bloginfo('name'); ?>" /><?php else: ?><?php bloginfo('name'); ?><?php endif; ?></a></<?php if(is_front_page()): ?>h1<?php else: ?>p<?php endif; ?>>

<form method="get" action="<?= home_url(); ?>/">
<ul id="searchArea" class="clearfix">
<li><input type="text" value="" name="s" id="s" /></li>
<li><input type="submit" value="" id="searchsubmit" /></li>
</ul>
</form>


<?php htmlspecialchars(get_option('sns-facebook-val')); ?>

<ul id="snsArea" class="clearfix">

<?php if(get_option('sns-facebook-val')): ?>
<li id="snsFb"><a href="https://www.facebook.com/<?= htmlspecialchars(get_option('sns-facebook-val')) ?>" target="_blank"><img src="<?= get_template_directory_uri(); ?>/lib/img/ico_facebook.png" width="8" height="17" alt="Facebook" /></a></li>
<?php endif; ?>

<?php if(get_option('sns-twitter-val')): ?>
<li id="snsTt"><a href="https://twitter.com/<?= htmlspecialchars(get_option('sns-twitter-val')) ?>" target="_blank"><img src="<?= get_template_directory_uri(); ?>/lib/img/ico_twitter.png" width="18" height="15" alt="Twitter" /></a></li>
<?php endif; ?>

<?php if(get_option('sns-google-val')): ?>
<li id="snsGp"><a href="https://plus.google.com/collection/<?= htmlspecialchars(get_option('sns-google-val')) ?>" target="_blank"><img src="<?= get_template_directory_uri(); ?>/lib/img/ico_google.png" width="25" height="16" alt="Google Plus" /></a></li>
<?php endif; ?>

<?php if(get_option('sns-feedly-val')): ?>
<li id="snsFl"><a href="<?= home_url(); ?>/<?= htmlspecialchars(get_option('sns-feedly-val')) ?>" target="_blank"><img src="<?= get_template_directory_uri(); ?>/lib/img/ico_feedly.png" width="18" height="16" alt="Feedly" /></a></li>
<?php endif; ?>

</ul>

</div><!-- end logoArea -->


<!-- start menuArea -->
<?php wp_nav_menu( array(
	'theme_location'=>'globalmenu', 
	'container'     =>'', 
	'menu_class'    =>'',
	'items_wrap'    =>'<div id="menuArea" class="clearfix"><div id="mobileMenu"><label for="menuSwitch">≡ MENU</label><input id="menuSwitch" type="checkbox"><ul id="topMenu">%3$s</ul></div></div>'));
?>
<!-- end menuArea -->

</div>
<!-- ===== head end ===== -->
