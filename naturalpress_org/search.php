<?php get_header(); ?>

<!-- ===== main start ===== -->

<div id="mainArea" class="wrap clearfix">

<div id="mainLt" class="noneMeta">



<!-- パン屑 start -->
<ul class="breadList clearfix">
<li><a href="http://test.pc-jozu.com">HOME</a>&nbsp;&gt;&nbsp;検索結果</li>
</ul>
<!-- パン屑 end -->


<h1>「<?php echo esc_html( $s ); ?>」の検索結果 <?php echo $wp_query->found_posts; ?> 件</h1>



<?php if ( have_posts() ) : ?>

	<?php while ( have_posts() ) : the_post(); ?>



	<?php get_template_part( 'content', 'listcate' ); ?>



	<?php endwhile; ?>



<!-- pager start -->
<?php get_template_part( 'content', 'pager' ); ?>
<!-- pager end -->



<?php else: ?>



<div id="entryArea">
<p>
一致する記事は見つかりませんでした。<br />
検索キーワードにタイプミスがないかご確認ください。<br />
</p>
<p>
上のメニューから目的のカテゴリを選んで移動、<br />
または別のキーワードで再度検索をするか、<br />
以下のURLから<?php bloginfo('name'); ?> WEBサイトのトップページへ移動出来ます。<br />
</p>
<p><a href="<?= home_url(); ?>"><?= home_url(); ?></a></p>

<br />
<?php get_search_form(); ?>

</div>



<?php endif; // end have_posts ?>




</div><!-- end mainLt -->


<?php get_sidebar(); ?>


</div><!-- end mainArea -->

<!-- ===== main end ===== -->


<?php get_footer(); ?>