<?php get_header(); ?>

<!-- ===== main start ===== -->

<div id="mainArea" class="wrap clearfix">

<div id="mainLt" class="noneMeta">



<!-- パン屑 start -->
<ul class="breadList clearfix">
<li><a href="http://test.pc-jozu.com">HOME</a>&nbsp;&gt;&nbsp;アーカイブ</li>
</ul>
<!-- パン屑 end -->

<?php if ( have_posts() ) : ?>




<?php if($monthnum || $year || $cat){ ?>
<h1><?php if($cat){ ?><?php single_cat_title(); ?><?php }elseif($monthnum||$year){ echo $year.'年'; ?><?php if($monthnum){ echo $monthnum.'月';} } ?>の記事</h1>
<?php } ?>



	<?php while ( have_posts() ) : the_post(); ?>



	<?php get_template_part( 'content', 'listcate' ); ?>



	<?php endwhile; ?>



<?php else: ?>



<h1>記事は見つかりませんでした</h1>


<div id="entryArea">
<p>
上のメニューから別のカテゴリを選んで移動するか、<br />
以下のURLから<?php bloginfo('name'); ?> WEBサイトのトップページへ移動出来ます。<br />
</p>
<p><a href="<?= home_url(); ?>"><?= home_url(); ?></a></p>

<br />
<?php get_search_form(); ?>

</div>



<?php endif; // end have_posts ?>



<!-- pager start -->
<?php get_template_part( 'content', 'pager' ); ?>
<!-- pager end -->



</div><!-- end mainLt -->


<?php get_sidebar(); ?>


</div><!-- end mainArea -->

<!-- ===== main end ===== -->


<?php get_footer(); ?>