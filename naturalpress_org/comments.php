<?php if(have_comments()): ?>
  <ol class="comments">
    <?php  wp_list_comments(); ?>
  </ol>
<?php endif; ?>

<?php
$commenter = wp_get_current_commenter();
$req = get_option( 'require_name_email' );
$aria_req = ( $req ? " aria-required='true'" : '' );
$comments_args = array(
  'title_reply' => 'コメントする',
  'label_submit' => __( 'Post Comment' ),
  'comment_field' => '<p class="comment-form-comment">' .
    '<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true">' .
    '</textarea></p>',
  'fields' => array(
    'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name' ) . ( $req ? ' <span class="required">*</span><br />' : '' ) . '</label> ' .
	                    '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
    'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email' ) . ( $req ? ' <span class="required">*</span><br />' : '' ) . '</label> ' .
	                    '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
    'url'    => '',
  ),
);
comment_form($comments_args);
?>