<?php get_header(); ?>

<!-- ===== main start ===== -->

<div id="mainArea" class="wrap clearfix">

<div id="mainLt">


<?php while ( have_posts() ) : the_post(); ?>


 
<!-- パン屑 start -->
<ul class="breadList clearfix">
<?php $cat = get_the_category(); ?>
<?php $cat = $cat[0]; ?>
<li><a href="<?= home_url(); ?>">HOME</a>&nbsp;&gt;&nbsp;<?php echo(get_category_parents($cat, true,'&nbsp;&gt;&nbsp;',false)); the_title(); ?></li>
</ul>
<!-- パン屑 end -->

<h1><?php the_title(); ?></h1>
<ul id="entryMeta" class="clearfix">
<li id="entryCate"><?php the_category('&nbsp;'); ?></li>
<li id="entryDate">投稿者：<?php the_author(); ?>&nbsp;｜&nbsp;<?php the_time(get_option('date_format')); ?></li>
</ul>
<div id="entryArea">


<?php if(get_post_meta($post->ID, 'postview1', true) || get_post_meta($post->ID, 'postview2', true) || get_post_meta($post->ID, 'postview3', true)) : ?>

<div id="pointArea">
<p class="pointTtl"><?php if(get_option('meta-pointttl-val')): ?><?= htmlspecialchars(get_option('meta-pointttl-val')); ?><?php else: ?>この記事を簡単に書くと…<?php endif; ?></p>
<ul>
<?php if(get_post_meta($post->ID, 'postview1', true)) : ?>
<li><?= htmlspecialchars(get_post_meta($post->ID, 'postview1', true)); ?></li>
<?php endif; ?>
<?php if(get_post_meta($post->ID, 'postview2', true)) : ?>
<li><?= htmlspecialchars(get_post_meta($post->ID, 'postview2', true)); ?></li>
<?php endif; ?>
<?php if(get_post_meta($post->ID, 'postview3', true)) : ?>
<li><?= htmlspecialchars(get_post_meta($post->ID, 'postview3', true)); ?></li>
<?php endif; ?>
</ul>
</div>

<?php endif; ?>

<?php dynamic_sidebar( 'main-widget1' ); ?>
<?php the_content(); ?>
<div class="tagArea"><?php the_tags( $before, $sep, $after ); ?></div>
<?php dynamic_sidebar( 'main-widget2' ); ?>

</div><!-- end entryArea -->

<?php comments_template(); ?>


<?php endwhile; ?>



</div><!-- end mainLt -->


<?php get_sidebar(); ?>


</div><!-- end mainArea -->

<!-- ===== main end ===== -->


<?php get_footer(); ?>