<div class="topItem">
<a href="<?php the_permalink(); ?>">
<div class="itemLt">
<p>
<?php if(has_post_thumbnail()) : ?>
<?php the_post_thumbnail(array(130)); ?>
<?php else: ?>
<img src="<?php echo get_template_directory_uri(); ?>/lib/img/noimg.png" width="100%" alt="" />
<?php endif; ?>
</p>
</div><!-- end itemLt -->
<div class="itemRt">
<div class="itemRtCont">
<p class="timest"><?php the_time(get_option('date_format')); ?></p>
<h3><?php the_title_attribute(); ?></h3>
<p class="mainTxt">
<?php
if(mb_strlen($post-> post_content,'UTF-8') > 100){
	$content= str_replace('\n', '', mb_substr(strip_tags($post-> post_content), 0, 100,'UTF-8'));
	echo $content.'…';
}else{
	echo str_replace('\n', '', strip_tags($post-> post_content));
}
?>
</p>
</div>
</div><!-- end itemRt -->
<p class="clear"></p>
</a>
</div><!-- end topitem -->