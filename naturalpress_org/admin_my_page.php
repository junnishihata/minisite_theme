<h2>NATURAL PRESS 設定画面</h2>

<?php $categories = get_categories(); ?>
<?php 
foreach($categories as $category) {  
 echo '<a href="' . get_category_link( $category->term_id ) . '" title="' . $category->name . '" ' . '>' . $category->name . '<br /><span' . $category->description .'</span></a></p>'; 
} 
 ?>
<form method="post" action="options.php">
      <?php 
        settings_fields( 'banner-settings-group' );
        do_settings_sections( 'banner-settings-group' );
      ?>
      <table class="form-table">
        <tbody>
          <tr>
            <th scope="row">
              <label for="banner_url">リンク</label>
            </th>
              <td><input type="text" id="banner_url" class="regular-text" name="banner_url" value="<?php echo get_option('banner_url'); ?>"></td>
          </tr>
          <tr>
            <th scope="row">
              <label for="banner_image">バナー画像</label>
            </th>
              <td><input type="text" id="banner_image" class="regular-text" name="banner_image" value="<?php echo get_option('banner_image'); ?>"></td>
          </tr>
        </tbody>
      </table>
      <?php submit_button(); ?>
    </form>
  </div>