<?php


/* ========== 公開画面 ========== */

// ヘッダ
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');

//メニュー挿入
register_nav_menu('globalmenu', 'グローバルメニュー');


/* ========== 管理画面 ========== */
/*
add_action ( 'admin_menu', 'mypress_add_pages' );

function mypress_add_pages () {
	add_menu_page('my PRESS 設定画面', 'NPtheme設定', 8, 'np-custom-admin.php', 'np_custom_page');
}

function np_custom_page() {
    include 'admin_my_page.php';
}
*/

// ビジュアルエディタ用CSS
//add_editor_style('lib/css/editor-style.css');

// アイキャッチ画像
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size(320, 180, true );


//トップページページング調整
if(get_option('top-newview-val') != ""){
	define('PAGE_1ST', get_option('top-newview-val'));
}else{
	define('PAGE_1ST', 5);
}

define('PAGE_2ND', get_option('posts_per_page'));
function top_paging($query) {

if(is_admin() || !$query -> is_main_query()){ return; }

if($query -> is_home()) {
	$query->set( 'posts_per_page', PAGE_1ST);
}
$paged = get_query_var('paged') ? intval(get_query_var('paged')): 1;
if($paged >= 2){
	$query -> set('offset', PAGE_1ST + PAGE_2ND * ($paged - 2));
	$query -> set('posts_per_page', PAGE_2ND);
	}
}
add_action('pre_get_posts', 'top_paging');


/* ----- ウィジェットエリア ----- */

// サイドバーのウィジェット
register_sidebar( array(
	'id' => 'side-widget',
	'name' => __( 'Sideber Area' ),
	'before_widget' => '<div class="widgetArea">',
	'after_widget' => '</div>',
	'before_title' => '<h3>',
	'after_title' => '</h3>',
) );

// フッターエリアのウィジェット
register_sidebar( array(
	'id' => 'footer-widget1',
	'name' => __( 'Footer Area1（フッタ左）' ),
	'before_widget' => '<div class="widgetArea">',
	'after_widget' => '</div>',
	'before_title' => '<h3>',
	'after_title' => '</h3>',
) );

register_sidebar( array(
	'id' => 'footer-widget2',
	'name' => __( 'Footer Area2（フッタ中）' ),
	'before_widget' => '<div class="widgetArea">',
	'after_widget' => '</div>',
	'before_title' => '<h3>',
	'after_title' => '</h3>',
) );

register_sidebar( array(
	'id' => 'footer-widget3',
	'name' => __( 'Footer Area3（フッタ右）' ),
	'before_widget' => '<div class="widgetArea">',
	'after_widget' => '</div>',
	'before_title' => '<h3>',
	'after_title' => '</h3>',
) );

// トップ広告枠1（上）
register_sidebar( array(
	'id' => 'top-widget1',
	'name' => __( 'トップ広告枠1（上）' ),
	'before_widget' => '<div class="adTopArea1">',
	'after_widget' => '</div>',
	'before_title' => '<h3>',
	'after_title' => '</h3>',
) );

// トップ広告枠2（中）
register_sidebar( array(
	'id' => 'top-widget2',
	'name' => __( 'トップ広告枠2（中）' ),
	'before_widget' => '<div class="adTopArea2">',
	'after_widget' => '</div>',
	'before_title' => '<h3>',
	'after_title' => '</h3>',
) );

// メイン広告枠1（上）
register_sidebar( array(
	'id' => 'main-widget1',
	'name' => __( 'メイン広告枠1（上）' ),
	'before_widget' => '<div class="adArea1">',
	'after_widget' => '</div>',
	'before_title' => '<h3>',
	'after_title' => '</h3>',
) );

// メイン広告枠1（上）
register_sidebar( array(
	'id' => 'main-widget1',
	'name' => __( 'メイン広告枠1（上）' ),
	'before_widget' => '<div class="adArea1">',
	'after_widget' => '</div>',
	'before_title' => '<h3>',
	'after_title' => '</h3>',
) );

// メイン広告枠2（下）
register_sidebar( array(
	'id' => 'main-widget2',
	'name' => __( 'メイン広告枠2（下）' ),
	'before_widget' => '<div class="adArea2">',
	'after_widget' => '</div>',
	'before_title' => '<h3>',
	'after_title' => '</h3>',
) );


/* ----- カスタマイザー ----- */

//テーマの基本色・ロゴ
function color_customizer($wp_customize){
$wp_customize -> add_section('theme-color',
	array(
	'title' => 'テーマの基本色・ロゴ',
	'priority' => 20,
	'description' => 'メニュー、タイトル等の色およびロゴを変更できます。'
	)
);

$wp_customize -> add_setting( 'theme-color-val',
	array(
	'type' => 'option',
	'default' => '#74c554'
	)
);
$wp_customize -> add_control(new WP_Customize_Color_Control(
	$wp_customize,
	'theme-color-ctr',
		array(
		'section' => 'theme-color',
		'settings' => 'theme-color-val',
		'label' => 'テーマカラーの選択'
		)
	)
);

$wp_customize -> add_setting('theme-logo-val',
	array(
	'type' => 'option',
	'default' => ''
	)
);
$wp_customize -> add_control(new WP_Customize_Image_Control(
	$wp_customize,
	'theme-logo-ctr',
		array(
		'section' => 'theme-color',
		'settings' => 'theme-logo-val',
		'label' => 'ロゴ画像を選択'
		)
	)
);

}

add_action('customize_register' , 'color_customizer');


//キャッチ、メタ情報など
function meta_customizer($wp_customize){
$wp_customize -> add_section('meta-info',
	array(
	'title' => 'キャッチ、メタ情報など',
	'priority' => 21,
	'description' => 'ヘッダーのキャッチコピーやメタ情報などを設定します。'
	)
);

$wp_customize -> add_setting('catch-copy-val',
	array(
	'type' => 'option',
	'default' => ''
	)
);
$wp_customize -> add_control(new WP_Customize_Control(
	$wp_customize,
	'catch-copy-ctr',
		array(
		'section' => 'meta-info',
		'settings' => 'catch-copy-val',
		'label' => 'キャッチコピー'
		)
	)
);


$wp_customize -> add_setting( 'meta-keyword-val',
	array(
	'type' => 'option',
	'default' => ''
	)
);
$wp_customize -> add_control(new WP_Customize_Control(
	$wp_customize,
	'meta-keyword-ctr',
		array(
		'section' => 'meta-info',
		'settings' => 'meta-keyword-val',
		'label' => 'キーワード(カンマ区切り)'
		)
	)
);

$wp_customize -> add_setting( 'meta-desc-val',
	array(
	'type' => 'option',
	'default' => ''
	)
);
$wp_customize -> add_control(new WP_Customize_Control(
	$wp_customize,
	'meta-desc-ctr',
		array(
		'section' => 'meta-info',
		'settings' => 'meta-desc-val',
		'label' => 'サイトの説明'
		)
	)
);

$wp_customize -> add_setting( 'meta-pointttl-val',
	array(
	'type' => 'option',
	'default'  => 'この記事をかんたんに書くと…',
	'default' => ''
	)
);
$wp_customize -> add_control(new WP_Customize_Control(
	$wp_customize,
	'meta-pointttl-ctr',
		array(
		'section' => 'meta-info',
		'settings' => 'meta-pointttl-val',
		'label' => '記事の概要タイトル'
		)
	)
);

$wp_customize -> add_setting( 'meta-copy-val',
	array(
	'type' => 'option',
	'default' => ''
	)
);
$wp_customize -> add_control(new WP_Customize_Control(
	$wp_customize,
	'meta-copy-ctr',
		array(
		'section' => 'meta-info',
		'settings' => 'meta-copy-val',
		'label' => 'コピーライト'
		)
	)
);

}

add_action('customize_register' , 'meta_customizer');


//Google アナリティクス
function analytics_customizer($wp_customize){
$wp_customize -> add_section('analytics-info',
	array(
	'title' => 'Google アナリティクス',
	'priority' => 22,
	'description' => 'Google アナリティクスのコードを入力。'
	)
);

$wp_customize -> add_setting('google-analytics-val',
	array(
	'type' => 'option',
	'default' => ''
	)
);
$wp_customize -> add_control(new WP_Customize_Control(
	$wp_customize,
	'google-analytics-ctr',
		array(
		'section' => 'analytics-info',
		'settings' => 'google-analytics-val',
		'label' => 'トラッキングID'
		)
	)
);

}

add_action('customize_register' , 'analytics_customizer');


//SNSアカウント
function sns_customizer($wp_customize){
$wp_customize -> add_section('sns-info',
	array(
	'title' => 'SNSアカウント',
	'priority' => 23,
	'description' => 'アイコンを表示するSNSのアカウントを入力。'
	)
);

$wp_customize -> add_setting('sns-facebook-val',
	array(
	'type' => 'option',
	'default' => ''
	)
);
$wp_customize -> add_control(new WP_Customize_Control(
	$wp_customize,
	'sns-facebook-ctr',
		array(
		'section' => 'sns-info',
		'settings' => 'sns-facebook-val',
		'label' => 'facebook'
		)
	)
);


$wp_customize -> add_setting( 'sns-twitter-val',
	array(
	'type' => 'option',
	'default' => ''
	)
);
$wp_customize -> add_control(new WP_Customize_Control(
	$wp_customize,
	'sns-twitter-ctr',
		array(
		'section' => 'sns-info',
		'settings' => 'sns-twitter-val',
		'label' => 'twitter'
		)
	)
);

$wp_customize -> add_setting( 'sns-google-val',
	array(
	'type' => 'option',
	'default' => ''
	)
);
$wp_customize -> add_control(new WP_Customize_Control(
	$wp_customize,
	'sns-google-ctr',
		array(
		'section' => 'sns-info',
		'settings' => 'sns-google-val',
		'label' => 'Google +'
		)
	)
);

$wp_customize -> add_setting( 'sns-feedly-val',
	array(
	'type' => 'option',
	'default' => ''
	)
);
$wp_customize -> add_control(new WP_Customize_Control(
	$wp_customize,
	'sns-feedly-ctr',
		array(
		'section' => 'sns-info',
		'settings' => 'sns-feedly-val',
		'label' => 'feedly'
		)
	)
);

}

add_action('customize_register' , 'sns_customizer');


//トップページ表示
function topview_customizer($wp_customize){
$wp_customize -> add_section('top-view',
	array(
	'title' => 'トップページ表示',
	'priority' => 24,
	'description' => 'トップページに表示するカテゴリを選択。'
	)
);

//新着記事数
$wp_customize -> add_setting('top-newview-val',
	array(
	'type' => 'option',
	'default' => 5
	)
);
$wp_customize -> add_control(new WP_Customize_Control(
	$wp_customize,
	'top-newview-ctr',
		array(
		'section' => 'top-view',
		'settings' => 'top-newview-val',
		'label' => '新着記事の投稿数',
		'type' => 'select',
		'choices' => array(
			0 => '選択',
			2 => 2,
			3 => 3,
			4 => 4,
			5 => 5,
			6 => 6,
			7 => 7,
			8 => 8,
			9 => 9,
			10 => 10,
			)
		)
	)
);

//カテゴリ名取得
$categories = get_categories('hide_empty=0');
$i = 0;
$catVal = array(0 => '選択');
foreach($categories as $category){
	$i++;
	$catVal[$category->slug] .= $category->name;
}

for($i=1; $i<=8; $i++){

	$wp_customize -> add_setting('top-view' . $i . '-val',
		array(
		'type' => 'option',
		'default' => 0
		)
	);
	$wp_customize -> add_control(new WP_Customize_Control(
		$wp_customize,
		'top-view' . $i . '-ctr',
			array(
			'section' => 'top-view',
			'settings' => 'top-view' . $i . '-val',
			'label' => 'カテゴリ' . $i . '',
			'type' => 'select',
			'choices' => $catVal
			)
		)
	);


	$wp_customize -> add_setting('top-view' . $i . 'num-val',
		array(
		'type' => 'option',
		'default' => 0
		)
	);
	$wp_customize -> add_control(new WP_Customize_Control(
		$wp_customize,
		'top-view' . $i . 'num-ctr',
			array(
			'section' => 'top-view',
			'settings' => 'top-view' . $i . 'num-val',
			'label' => 'カテゴリ' . $i . 'の投稿数',
			'type' => 'select',
			'choices' => array(
				0 => '選択',
				1 => 1,
				2 => 2,
				3 => 3,
				4 => 4,
				5 => 5
				)
			)
		)
	);

}


}

add_action('customize_register' , 'topview_customizer');


//メイン写真
function mainph_customizer($wp_customize){
$wp_customize -> add_section('main-ph',
	array(
	'title' => 'メイン写真',
	'priority' => 25,
	'description' => 'メイン写真をアップロード（1020px×280px 推奨）'
	)
);

$wp_customize -> add_setting('main-ph-val',
	array(
	'type' => 'option',
	'default' => ''
	)
);
$wp_customize -> add_control(new WP_Customize_Image_Control(
	$wp_customize,
	'main-ph-ctr',
		array(
		'section' => 'main-ph',
		'settings' => 'main-ph-val',
		'label' => 'メイン写真を選択'
		)
	)
);

}

add_action('customize_register' , 'mainph_customizer');


//おすすめ記事
function recom_customizer($wp_customize){
$wp_customize -> add_section('recom-post',
	array(
	'title' => 'おすすめ記事',
	'priority' => 26,
	'description' => 'トップページにおすすめの記事を表示します。'
	)
);

$wp_customize -> add_setting('recom-post-val',
	array(
	'type' => 'option',
	'default' => ''
	)
);
$wp_customize -> add_control(new WP_Customize_Control(
	$wp_customize,
	'recom-post-ctr',
		array(
		'section' => 'recom-post',
		'settings' => 'recom-post-val',
		'label' => '記事IDをカンマ区切り（半角数字）'
		)
	)
);

}

add_action('customize_register' , 'recom_customizer');


function customize_css(){

	$customizer_color = get_option('theme-color-val');

	if($customizer_color != ""){
		echo '<style type="text/css">
	<!--
	#headArea { border-top: 4px solid ' . $customizer_color . ' !important; }
	#searchsubmit { background-color: ' . $customizer_color . ' !important; }
	#menuArea { background-color: ' . $customizer_color . ' !important; }
	#topMenu li ul li { background-color: ' . $customizer_color . ' !important; }
	#mainRt h3 { background-color: ' . $customizer_color . ' !important; }
	#entryArea h2 { border-left: 5px solid ' . $customizer_color . ' !important; }
	#entryArea h3 { background-color: ' . $customizer_color . ' !important; }
	-->
	</style>';
	}

}
add_action( 'wp_head', 'customize_css');



/* ----- 投稿画面　カスタムフィールド ----- */


//メタ・記事の概要

function ins_metaseo() {
	global $post;
	wp_nonce_field(wp_create_nonce(__FILE__), 'metaseo_nonce');
	echo '<p>
<label for="metakey">キーワード(カンマ区切り)　（meta keyword）</label><br />
<input type="text" name="metakey" id="metakey" size="60" value="' . htmlspecialchars(get_post_meta($post->ID, 'metakey', true)) . '" />
</p>

<p>
<label for="metadesc">ページの説明　（meta description）</label><br />
<input type="text" name="metadesc" id="metadesc" size="60" value="' . htmlspecialchars(get_post_meta($post->ID, 'metadesc', true)) . '" />
</p>

<p>
<label for="postview1">記事の概要1</label><br />
<input type="text" name="postview1" id="postview1" size="60" value="' . htmlspecialchars(get_post_meta($post->ID, 'postview1', true)) . '" />
</p>

<p>
<label for="postview2">記事の概要2</label><br />
<input type="text" name="postview2" id="postview2" size="60" value="' . htmlspecialchars(get_post_meta($post->ID, 'postview2', true)) . '" />
</p>

<p>
<label for="postview3">記事の概要3</label><br />
<input type="text" name="postview3" id="postview3" size="60" value="' . htmlspecialchars(get_post_meta($post->ID, 'postview3', true)) . '" />
</p>';

}

function add_metaseo() {
	add_meta_box('works_info', 'メタ・記事の概要', 'ins_metaseo', 'post', 'normal', 'high');
}

function save_metaseo($post_id){
	$metaseo_nonce = isset($_POST['metaseo_nonce']) ? $_POST['metaseo_nonce'] : null;
	if(!wp_verify_nonce($metaseo_nonce, wp_create_nonce(__FILE__))) {
		return $post_id;
	}
	if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { return $post_id; }
	if(!current_user_can('edit_post', $post_id)) { return $post_id; }

	update_post_meta($post_id, 'metakey', $_POST['metakey']);
	update_post_meta($post_id, 'metadesc', $_POST['metadesc']);
	update_post_meta($post_id, 'postview1', $_POST['postview1']);
	update_post_meta($post_id, 'postview2', $_POST['postview2']);
	update_post_meta($post_id, 'postview3', $_POST['postview3']);
}

add_action('admin_menu', 'add_metaseo');
add_action('save_post', 'save_metaseo');

?>
