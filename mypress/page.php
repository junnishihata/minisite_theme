<?php get_header(); ?>

<!-- ===== main start ===== -->

<div id="mainArea" class="wrap clearfix">

<div id="mainLt" class="noneMeta">


<?php while ( have_posts() ) : the_post(); ?>



<!-- パン屑 start -->
<ul class="breadList clearfix">
<li><a href="<?= home_url(); ?>">HOME</a>&nbsp;&gt;&nbsp;<?php foreach ( array_reverse(get_post_ancestors($post->ID)) as $parid ) { ?><a href="<?php echo get_page_link( $parid );?>"><?php echo get_page($parid)->post_title; ?></a>&nbsp;&gt;&nbsp;<?php } ?><?php the_title(''); ?></li>
</ul>
<!-- パン屑 end -->

<h1><?php the_title(); ?></h1>

<div id="entryArea">
<?php the_content(); ?>
</div><!-- end entryArea -->


<?php endwhile; ?>


</div><!-- end mainLt -->


<?php get_sidebar(); ?>


</div><!-- end mainArea -->

<!-- ===== main end ===== -->


<?php get_footer(); ?>