<div class="pager">
	<?php global $wp_rewrite;
	$paginate_base = get_pagenum_link(1);
	if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
		$paginate_format = '';
		$paginate_base = add_query_arg('paged','%#%');
	}
	else{
		$paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
		user_trailingslashit('page/%#%/','paged');;
		$paginate_base .= '%_%';
	}

//トップページ調整
if(is_home()){
	$adPgNum = get_option('posts_per_page');

	if(get_option('top-newview-val') != ""){
		$tpPgNum = get_option('top-newview-val');
	}else{
		$tpPgNum = 5;
	}

	$diff = $adPgNum - $tpPgNum;
	$postNum = wp_count_posts() -> publish;
	$lowNum = ($wp_query->max_num_pages * $adPgNum) - $diff;
	$highNum = ($wp_query->max_num_pages * $adPgNum);

	if($lowNum < $postNum && $highNum >= $postNum){
		$totalNum = $wp_query->max_num_pages + 1;
	}else{
		$totalNum = $wp_query->max_num_pages;
	}
}else{
	$totalNum = $wp_query->max_num_pages;
}

	echo paginate_links(array(
		'base' => $paginate_base,
		'format' => $paginate_format,
		'total' => $totalNum,
		'mid_size' => 4,
		'current' => ($paged ? $paged : 1),
		'prev_text' => '≪',
		'next_text' => '≫',
	)); ?>
</div>