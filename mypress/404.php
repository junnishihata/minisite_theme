<?php get_header(); ?>

<!-- ===== main start ===== -->

<div id="mainArea" class="wrap clearfix">

<div id="mainLt" class="noneMeta">



<!-- パン屑 start -->
<ul class="breadList clearfix">
<li><a href="">HOME</a>&nbsp;&gt;&nbsp;Not found</li>
</ul>
<!-- パン屑 end -->

<h1>ページが見つかりません</h1>

<div id="entryArea">
<p>
指定されたページは、このサーバ上に存在しません。<br />
他の場所に移動したか、または削除された可能性があります。<br />
ご指定のURLにタイプミスがないかご確認ください。<br />
</p>
<p>
上のメニューから目的のカテゴリを選んで移動するか、<br />
以下のURLから<?php bloginfo('name'); ?> WEBサイトのトップページへ移動出来ます。<br />
</p>
<p><a href="<?= home_url(); ?>"><?= home_url(); ?></a></p>
</div>



</div><!-- end mainLt -->


<?php get_sidebar(); ?>


</div><!-- end mainArea -->

<!-- ===== main end ===== -->


<?php get_footer(); ?>
