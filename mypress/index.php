<?php get_header(); ?>

<!-- ===== main start ===== -->


<?php if(!is_paged()): ?>


	<?php if(get_option('main-ph-val')): ?>
	<div class="wrap pdt20 mainPh"><img src="<?= htmlspecialchars(get_option('main-ph-val')) ?>" width="100%" alt="" /></div>
	<?php endif; ?>



	<?php $recomArr = explode(",", htmlspecialchars(get_option('recom-post-val')));	?>
	<?php if($recomArr[0] != ""): ?>
	<!-- おすすめ記事 start -->


		<div class="wrap recomTtl"><h2>おすすめ記事</h2></div>

		<div class="recomSpace clearfix">
		<div class="recomContainer">
		<ul class="recomArea">



	<?php while ( $i < count($recomArr) ) : $i++; ?>
	<?php $post = get_post($recomArr[$i-1], 'OBJECT', 'raw'); ?>


	<li>
	<a href="<?php the_permalink(); ?>">
	<p class="categoriTxt"><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></p>
	<p class="itemTxt"><?php the_title(); ?></p>
	<p>
	<?php if(has_post_thumbnail()) : ?>
	<?php the_post_thumbnail(array(320,180)); ?>
	<?php else: ?>
	<img src="<?php echo get_template_directory_uri(); ?>/lib/img/noimg_b.png" width="100%" alt="" />
	<?php endif; ?>
	</p>
	</a>
	</li>

	<?php endwhile; ?>

		</ul>

		</div><!-- end recomContainer -->
		</div><!-- end recomSpace -->


	<!-- おすすめ記事 end -->
	<?php endif; ?>


<?php endif; ?>



<div id="mainArea" class="wrap clearfix">

<div id="mainLt">


<?php dynamic_sidebar( 'top-widget1' ); ?>


<?php if(!is_paged()): ?>


	<!-- newpost start -->

	<h2>新着記事</h2>

	<?php if ( have_posts() ) : ?>


		<?php $postNum = 0; ?>

		<?php while ( have_posts() ) : the_post(); ?>

		<?php $postNum++; ?>

	<?php if($postNum == 1) : ?>

		<div class="col2 clearfix">
		<div class="topItem fl">
		<a href="<?php the_permalink(); ?>">
		<p class="categoriTxt"><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></p>
		<p>
		<?php if(has_post_thumbnail()) : ?>
		<?php the_post_thumbnail(); ?>
		<?php else: ?>
		<img src="<?php echo get_template_directory_uri(); ?>/lib/img/noimg_b.png" width="100%" alt="" />
		<?php endif; ?>
		</p>
		<p class="timest"><?php the_time(get_option('date_format')); ?></p>
		<h3><?php the_title_attribute(); ?></h3>
		<p class="mainTxt">
		<?php
		if(mb_strlen($post-> post_content,'UTF-8') > 60){
			$content= str_replace('\n', '', mb_substr(strip_tags($post-> post_content), 0, 60,'UTF-8'));
			echo $content.'…';
		}else{
			echo str_replace('\n', '', strip_tags($post-> post_content));
		}
		?>
		</p>
		</a>
		</div><!-- end topitem -->

	<?php elseif($postNum == 2): ?>

		<div class="topItem fr">
		<a href="<?php the_permalink(); ?>">
		<p class="categoriTxt"><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></p>
		<p>
		<?php if(has_post_thumbnail()) : ?>
		<?php the_post_thumbnail(); ?>
		<?php else: ?>
		<img src="<?php echo get_template_directory_uri(); ?>/lib/img/noimg_b.png" width="100%" alt="" />
		<?php endif; ?>
		</p>
		<p class="timest"><?php the_time(get_option('date_format')); ?></p>
		<h3><?php the_title_attribute(); ?></h3>
		<p class="mainTxt">
		<?php
		if(mb_strlen($post-> post_content,'UTF-8') > 60){
			$content= str_replace('\n', '', mb_substr(strip_tags($post-> post_content), 0, 60,'UTF-8'));
			echo $content.'…';
		}else{
			echo str_replace('\n', '', strip_tags($post-> post_content));
		}
		?>
		</p>
		</a>
		</div><!-- end topitem -->
		</div><!-- end col2 -->

	<?php elseif($postNum >= 3): ?>

			<?php get_template_part( 'content', 'listcate' ); ?>

	<?php endif; ?>

	<?php if(wp_count_posts() -> publish == 1 || get_option('top-newview-val') == 1) : ?>
		</div><!-- end col2 -->
	<?php endif; ?>


		<?php endwhile; ?>

	<?php
	if(get_option('top-newview-val') != ""){
		$tpPgNum = get_option('top-newview-val');
	}else{
		$tpPgNum = 5;
	}
	?>
	<?php if($tpPgNum < wp_count_posts() -> publish): ?>
	<div class="morePost"><a href="<?= home_url(); ?>/?paged=2">その他の記事を見る</a></div>
	<?php endif; ?>


	<?php endif; // end have_posts ?>

	<!-- newpost end -->


	<?php dynamic_sidebar('top-widget2'); ?>



	<!-- newpost start -->

	<?php for($i=1; $i<=8; $i++) : ?>
	<?php if(get_option('top-view' . $i . '-val') && get_option('top-view' . $i . 'num-val')): ?>

		<?php query_posts("category_name=" . htmlspecialchars(get_option('top-view' . $i . '-val')) . "&showposts=" . htmlspecialchars(get_option('top-view' . $i . 'num-val')) . ""); ?>
		<?php if( have_posts()): ?>


		<h2><?php single_term_title(); ?></h2>


			<?php $postNum = 0; ?>

			<?php while(have_posts()): the_post(); ?>

			<?php $postNum++; ?>


		<?php if($postNum == 1): ?>

			<div class="topItem itemBig">
			<a href="<?php the_permalink(); ?>">
			<div class="itemLt">
			<p>
			<?php if(has_post_thumbnail()) : ?>
			<?php the_post_thumbnail(); ?>
			<?php else: ?>
			<img src="<?php echo get_template_directory_uri(); ?>/lib/img/noimg_b.png" width="100%" alt="" />
			<?php endif; ?>
			</p>
			</div><!-- end itemLt -->
			<div class="itemRt">
			<p class="timest"><?php the_time(get_option('date_format')); ?></p>
			<h3><?php the_title_attribute(); ?></h3>
			<p class="mainTxt">
			<?php
			if(mb_strlen($post-> post_content,'UTF-8') > 60){
				$content= str_replace('\n', '', mb_substr(strip_tags($post-> post_content), 0, 60,'UTF-8'));
				echo $content.'…';
			}else{
				echo str_replace('\n', '', strip_tags($post-> post_content));
			}
			?>
			</p>
			</div><!-- end itemRt -->
			<p class="clear"></p>
			</a>
			</div><!-- end topitem -->

		<?php elseif($postNum >= 2): ?>

			<?php get_template_part( 'content', 'list' ); ?>

		<?php endif; ?>

			<?php endwhile; ?>


			<?php
			$cate = get_category_by_slug(htmlspecialchars(get_option('top-view' . $i . '-val')));
			$cateNum = get_category($cate->cat_ID) -> category_count;
			$cateLink = get_category_link($cate->cat_ID);
			?>
			<?php if($cateNum > htmlspecialchars(get_option('top-view' . $i . 'num-val'))): ?>
			<div class="morePost"><a href="<?= $cateLink; ?>">その他の記事を見る</a></div>
			<?php endif; ?>


		<?php endif; // end have_posts ?>
		<?php wp_reset_query(); ?>

	<?php endif; ?>
	<?php endfor; ?>
	<!-- newpost end -->


<?php else: ?>


	<?php if ( have_posts() ) : ?>

	<div class="topListPage">

		<?php while ( have_posts() ) : the_post(); ?>


		<?php get_template_part( 'content', 'listcate' ); ?>


		<?php endwhile; ?>



	<?php else: ?>



	<h1>記事は見つかりませんでした</h1>


	<div id="entryArea">
	<p>
	上のメニューから別のカテゴリを選んで移動するか、<br />
	以下のURLから<?php bloginfo('name'); ?> WEBサイトのトップページへ移動出来ます。<br />
	</p>
	<p><a href="<?= home_url(); ?>"><?= home_url(); ?></a></p>

	<br />
	<?php get_search_form(); ?>

	</div>


	<?php endif; // end have_posts ?>


	<!-- pager start -->
	<?php get_template_part( 'content', 'pager' ); ?>
	<!-- pager end -->

	</div><!-- end topListPage -->

<?php endif; ?>



</div><!-- end mainLt -->


<?php get_sidebar(); ?>


</div><!-- end mainArea -->

<!-- ===== main end ===== -->


<?php get_footer(); ?>