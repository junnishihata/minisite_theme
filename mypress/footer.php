
<!-- ===== foot start ===== -->
<div id="footBg">
<div id="footArea" class="wrap clearfix">

<div class="footContents clearfix">
<div class="col3">

<?php dynamic_sidebar( 'footer-widget1' ); ?>

</div>

<div class="col3">

<?php dynamic_sidebar( 'footer-widget2' ); ?>

</div>

<div class="col3">

<?php dynamic_sidebar( 'footer-widget3' ); ?>

</div>
</div>

<div class="clearfix">  
<p id="copyright">
<?php if(get_option('meta-copy-val')): ?>
<?= htmlspecialchars(get_option('meta-copy-val')); ?>
<?php endif; ?>
</p>
<p id="mypress"><a href="" target="_blank" rel="nofollow"></a></p>
</div>

</div><!-- end footArea -->
</div><!-- end footBg -->
<!-- ===== foot end ===== -->


<?php wp_footer(); ?>

</body>
</html>
